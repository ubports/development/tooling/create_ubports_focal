import os
from typing import NamedTuple

from gql import Client, GraphQLRequest, gql
from gql.client import SyncClientSession
from gql.transport.requests import RequestsHTTPTransport

GITLAB_GRAPHQL_API = 'https://gitlab.com/api/graphql'

class GitlabRepo(NamedTuple):
    name: str
    branches: set[str]

def get_gql_client():
    try:
        headers = {
            'Authorization': f'Bearer {os.environ["GITLAB_API_TOKEN"]}'
        }
    except KeyError as e:
        raise RuntimeError('Please provide GitLab API token.') from e

    transport = RequestsHTTPTransport(GITLAB_GRAPHQL_API, headers=headers)
    return Client(transport=transport, fetch_schema_from_transport=True)

def fetch_repos_and_branches():
    query = gql("""
        query repoBranches($cursor: String) {
          group(fullPath: "ubports/development/core") {
            projects(
              includeSubgroups: true,
              first: 32, # Maximum without exceeding complexity budget for
                         # authenticated user.
              after: $cursor,
            ) {
              edges {
                cursor
                node {
                  fullPath
                  archived
                  repository {
                    branchNames(searchPattern: "*", offset: 0, limit: 100)
                  }
                }
              }
              pageInfo {
                hasNextPage
              }
            }
          }
        }
    """)

    nextpage_cursor: str | None = None
    with get_gql_client() as session:
        assert isinstance(session, SyncClientSession)

        while True:
            print('.', end='', flush=True)
            result = session.execute(query, variable_values={'cursor': nextpage_cursor})
            nextpage_cursor = None

            for edge in result['group']['projects']['edges']:
                nextpage_cursor = edge['cursor']

                if edge['node']['archived']:
                    continue

                yield GitlabRepo(
                    name=edge['node']['fullPath'],
                    branches=set(edge['node']['repository']['branchNames']))

            if not result['group']['projects']['pageInfo']['hasNextPage']:
                break

            # If the edges array is empty, then nothing will set the nextpage_cursor.
            # This is just in case.
            if nextpage_cursor is None:
                break

    print("")

DEV_BRANCHES = set([ 'main', 'ubports/latest', 'ubports/contrib' ])
FOCAL_BRANCH = set([ 'ubports/focal' ])

def action_for_repo(repo: GitlabRepo):
    dev_branches = repo.branches.intersection(DEV_BRANCHES)
    focal_branch = repo.branches.intersection(FOCAL_BRANCH)

    if len(dev_branches) == 1 and len(focal_branch) == 0:
        # From, to
        return (( dev_branches.pop(), 'ubports/focal' ),
                'creates Focal from main', False)
    elif len(focal_branch) == 1 and len(dev_branches) == 0:
        if repo.name.find('packaging/') == -1:
            return (( focal_branch.pop(), 'main' ),
                    'creates main from Focal branch', False)
        else:
            return (None, 'packaging repo with Focal only needs special care', False)
    elif len(focal_branch) == 1 and len(dev_branches) >= 1:
        return (None, 'both Focal and main exists (probably the script has run on them)', True)
    elif len(dev_branches) > 1:
        return (None, 'more than 1 development branches???', True)
    else:
        return (None, 'unknown repo state ', True)

def execute_actions(actions: list[tuple[GitlabRepo, tuple[str, str]]]):
    query = gql('''
        mutation createBranch(
          $projectPath: ID!,
          $name: String!,
          $ref: String!,
        ) {
          createBranch(
            input: {
              projectPath: $projectPath,
              name: $name,
              ref: $ref,
            }
          ) {
            errors
          }
        }
    ''')

    batch_size = 5
    with get_gql_client() as session:
        assert isinstance(session, SyncClientSession)

        for i in range(0, len(actions), batch_size):
            [print(f'{repo.name.split("/")[-1]}: {action[0]} -> {action[1]}')
                for (repo, action) in actions[i:i + batch_size]]

            reqs: list[GraphQLRequest] = [GraphQLRequest(query, {
                'projectPath': repo.name,
                'ref': action[0],
                'name': action[1],
            }) for (repo, action) in actions[i:i + batch_size]]

            results = session.execute_batch(reqs, get_execution_result=False)

            found_error = False
            for j in range(0, len(results)):
                try:
                    errors = results[j]['errors']
                    repo, _ = actions[i + j]
                    print(f'{repo.name.split("/")[-1]}: {errors}')
                    found_error = True
                except KeyError:
                    pass

            if found_error:
                break

def main():
    reason_map = {}

    for repo in fetch_repos_and_branches():
        action_reason = action_for_repo(repo)
        action, reason, _ = action_reason

        try:
            reason_map[action_reason].append(repo)
        except KeyError:
            reason_map[action_reason] = [repo]

    for (action_reason, repos) in reason_map.items():
        action, reason, dump = action_reason

        if action is not None:
            print(f'Will branch {action[1]} from {action[0]} because: {reason} ({len(repos)})')
        else:
            print(f'Will do nothing because: {reason} ({len(repos)})')

        if not dump:
            [print(f'  - {repo.name}') for repo in repos]
        else:
            [print(f'  - {repo.name} {repo.branches}') for repo in repos]

    answer = input("Do you want to execute these changes [y/n]: ")
    if answer != 'y':
        print("Not confirmed, exiting")
        quit(1)

    actions = []
    for (action_reason, repos) in reason_map.items():
        action, reason, dump = action_reason
        if action is not None:
            actions = actions + [(repo, action) for repo in repos]

    execute_actions(actions)

    print("Done.")

if __name__ == '__main__':
    main()
